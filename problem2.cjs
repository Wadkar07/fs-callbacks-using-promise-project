const fs = require('fs');

function readFile(filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}

function writeFile(fileName, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(fileName, data, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(fileName);
            }
        });
    });
}

function deleteFiles(index,fileNames){
    return new Promise((resolve,reject)=>{
        if (index < fileNames.length) {
            fs.unlink(__dirname+'/'+fileNames[index], (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    deleteFiles(index + 1, fileNames);
                }
            });
        }
        else {
            resolve();
        }
    })
}



function readWriteAndDeleteFileUsingFs() {
    let filesCreated = [];
    readFile('lipsum.txt')
        .then((data) => {
            let uppererCasedata = data.toUpperCase();
            return writeFile('lipsumUpperCase.txt', uppererCasedata);
        })
        .then((filename) => {
            filesCreated.push(filename);
            return readFile(filename);
        })
        .then((data) => {
            let lowerCaseSortedData = data
                .toLowerCase()
                .split('.')
                .join('\n');
            return writeFile('lipsumLowerCase.txt', lowerCaseSortedData);
        })
        .then((filename) => {
            filesCreated.push(filename);
            return readFile(filename);
        })
        .then((data) => {
            let sortedContent = data.split('.').sort().join('\n');
            return writeFile('lipsumSortedData.txt', sortedContent);
        })
        .then((filename) => {
            filesCreated.push(filename);
            let fileContent = filesCreated.join('\n');
            return writeFile('FileCreated.txt', fileContent);
        })
        .then((filename) => {
            return readFile(filename);
        })
        .then((fileNames)=>{
            deleteFiles(0,fileNames.split('\n'));
        })
        .catch((err) => {
            console.log(err);
        })

}

module.exports = readWriteAndDeleteFileUsingFs;