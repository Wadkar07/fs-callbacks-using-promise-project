const fs = require('fs');
const path = require('path');

function createDirectory(directoryName) {
    return new Promise((resolve, reject) => {
        let directory = path.join(__dirname, directoryName);
        fs.mkdir(directory, (err) => {
            if (err && err.code !== 'EEXIST') {
                reject(err);
            } else {
                resolve(directory);
            }

        });
    });
}


function createFiles(index, randomJsonFileNames) {
    return new Promise((resolve, reject) => {
        if (index < randomJsonFileNames.length) {
            let fileContent = {
                name: randomJsonFileNames[index],
                fileNo: index + 1
            }
            fs.writeFile(randomJsonFileNames[index], JSON.stringify(fileContent), (err) => {
                if (err) {
                    reject(err);
                }
            });
            createFiles(index + 1, randomJsonFileNames);
        }
        else {
            resolve();
        }
    });
}

function deleteFiles(index, randomJsonFileNames) {
    return new Promise((resolve, reject) => {
        if (index < randomJsonFileNames.length) {
            fs.unlink(randomJsonFileNames[index], (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    deleteFiles(index + 1, randomJsonFileNames);
                }
            });
        }
        else {
            resolve();
        }
    });
}

function createAndDeleteDirectoryAndFiles(directory = 'RandomJsonFiles') {
    createDirectory(directory)
        .then((directoryName) => {
            const minimumFilesToBeCreated = 1;
            const randomNumber = Math.floor(Math.random() * 10) + minimumFilesToBeCreated;
            const randomJsonFileNames = Array.from({ length: randomNumber }, (value, index) => {
                return `${directoryName}/File_${index + 1}.json`;
            });
            return randomJsonFileNames;
        })
        .then((randomJsonFileNames) => {
            createFiles(0, randomJsonFileNames);
            return randomJsonFileNames;
        })
        .then((randomJsonFileNames) => {
            deleteFiles(0, randomJsonFileNames);
        })
        .catch((err) => {
            console.log(err);
        });
}

module.exports = createAndDeleteDirectoryAndFiles;